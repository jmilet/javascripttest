import BaseHTTPServer
import json
import random

class Handler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_GET(request):
        if request.path == '/data':
            request.send_response(200)
            request.send_header("Content-type", "application/json")
            request.end_headers()
            request.wfile.write(json.dumps({'value': int(random.random() * 100)}))
        elif request.path == '/':
            with open("client.html", "rt") as f:
                request.send_response(200)
                request.send_header("Content-type", "text/html")
                request.end_headers()
                request.wfile.write("".join(f.readlines()))

if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer

    httpd = server_class(('127.0.0.1', 9000), Handler)

    try:

        httpd.serve_forever()
        s.write(str(int(random.random() * 100)))

    except KeyboardInterrupt:
        pass
        httpd.server_close()
